package com.company;

public class AtBashCipher implements Cipher {
    String[] dictionary = new String[26];

    public AtBashCipher() {
        char firstletter = 'a';
        for(int i=0; i<26; i++){
            this.dictionary[i] = Character.toString(firstletter++);
        }
    }

    @Override
    public String decode(String message) {
        return encode(message);
    }

    @Override
    public String encode(String message) {
        String[] split = message.split("");
        String finalMessage = "";
        for(String character : split){
            for(int i=0; i<26; i++){
                if(character.equals(this.dictionary[i])){
                    finalMessage = finalMessage + this.dictionary[25-i];
                    break;
                }
            }
        }
        return finalMessage;

    }


}
